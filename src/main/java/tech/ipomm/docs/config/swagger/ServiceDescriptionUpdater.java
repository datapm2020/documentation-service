package tech.ipomm.docs.config.swagger;

import java.time.LocalDate;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import tech.ipomm.traefik.api.TraefikClient;

@Component
public class ServiceDescriptionUpdater {

	private static final Logger logger = LoggerFactory.getLogger(ServiceDescriptionUpdater.class);
	private final String DEFAULT_SWAGGER_URL;
	private final String GATEWAY_HOSTNAME;
	private final String GATEWAY_PROTOCOL;
	private final String SERVICE_SUFFIX;

	private final RestTemplate template;

	public ServiceDescriptionUpdater(@Value("${swagger.default-path}") String DEFAULT_SWAGGER_URL,
			@Value("${traefik-gateway.hostname}") String GATEWAY_HOSTNAME,
			@Value("${traefik-gateway.protocol}") String GATEWAY_PROTOCOL,
			@Value("${traefik-api.service-suffix}") String SERVICE_SUFFIX) {
		this.template = new RestTemplate();
		this.DEFAULT_SWAGGER_URL = DEFAULT_SWAGGER_URL;
		this.GATEWAY_PROTOCOL = GATEWAY_PROTOCOL;
		this.GATEWAY_HOSTNAME = GATEWAY_HOSTNAME;
		this.SERVICE_SUFFIX = SERVICE_SUFFIX;
	}

	@Autowired
	private ServiceDefinitionsContext definitionContext;
	@Autowired
	private TraefikClient traefikClient;

	public void refreshSwaggerConfigurations(Optional<String> subdomain) {
		logger.debug("Starting Service Definition Context refresh");
		definitionContext.getSwaggerDefinitions().clear();
		traefikClient.findHttpRouters().stream().map(r -> r.getService())
				.filter(r -> r.toLowerCase().endsWith(SERVICE_SUFFIX.toLowerCase())).distinct().forEach(serviceName -> {
					logger.debug("Attempting service definition refresh for Service : {} ", serviceName);
					String openApiUrl = getOpenApiURL(serviceName, subdomain);
					Optional<Object> jsonData = getSwaggerDefinitionForAPI(serviceName, openApiUrl);
					if (jsonData.isPresent()) {
						definitionContext.addServiceDefinition(serviceName, openApiUrl);
					} else {
						logger.warn("Skipping service id : {} Error : Could not get Swagegr definition from API ",
								serviceName);
					}
				});

		logger.info("Service Definition Context Refreshed at :  {}", LocalDate.now());
	}

	private String getOpenApiURL(String serviceName, Optional<String> subdomain) {
		if (subdomain.isPresent()) 
			return GATEWAY_PROTOCOL + "://" + subdomain.get() + "." + GATEWAY_HOSTNAME + "/" + serviceName + DEFAULT_SWAGGER_URL;
		else
			return GATEWAY_PROTOCOL + "://"+ GATEWAY_HOSTNAME + "/" + serviceName + DEFAULT_SWAGGER_URL;
	}

	private Optional<Object> getSwaggerDefinitionForAPI(String serviceName, String url) {
		logger.info("Accessing the SwaggerDefinition JSON for Service : {} : URL : {} ", serviceName, url);
		try {
			Object jsonData = template.getForObject(url, Object.class);
			return Optional.of(jsonData);
		} catch (RestClientException ex) {
			logger.error("Error while getting service definition for service : {} Error : {} ", serviceName,
					ex.getMessage());
			return Optional.empty();
		}

	}

}
