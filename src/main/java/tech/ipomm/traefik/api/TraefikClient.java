package tech.ipomm.traefik.api;

import java.util.Collection;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name="traefik-api",url="${traefik-api.ribbon.listOfServers}")
public interface TraefikClient {
   @RequestMapping(method = RequestMethod.GET, value = "/http/routers")
   public Collection<HttpRouter> findHttpRouters();
}