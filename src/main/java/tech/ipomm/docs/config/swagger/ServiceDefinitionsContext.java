package tech.ipomm.docs.config.swagger;

import java.util.HashSet;
import java.util.Set;
import org.springdoc.core.AbstractSwaggerUiConfigProperties.SwaggerUrl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ServiceDefinitionsContext {

	private final Set<SwaggerUrl> serviceDescriptions;

	private ServiceDefinitionsContext() {
		serviceDescriptions = new HashSet<>();
	}

	public void addServiceDefinition(String serviceName, String apiUrl) {
		serviceDescriptions.add(new SwaggerUrl(serviceName, apiUrl));
	}

	public void addServiceDefinition(SwaggerUrl swaggerUrl) {
		serviceDescriptions.add(swaggerUrl);
	}

	public Set<SwaggerUrl> getSwaggerDefinitions() {
		return this.serviceDescriptions;
	}
}
