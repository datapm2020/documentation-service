package tech.ipomm.docs.config.swagger;


import java.util.Set;

import org.springdoc.core.AbstractSwaggerUiConfigProperties.SwaggerUrl;
import org.springdoc.core.SwaggerUiConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;




@Configuration
public class SwaggerUIConfiguration {
	
	@Autowired
	private ServiceDefinitionsContext definitionContext;
	
	
    @Primary
    @Bean
    public SwaggerUiConfigProperties swaggerResourcesProvider(SwaggerUiConfigProperties swaggerUiConfigProperties) {  
            Set<SwaggerUrl> swaggerUrls = definitionContext.getSwaggerDefinitions();
            swaggerUiConfigProperties.setUrls(swaggerUrls);
            return swaggerUiConfigProperties;
        
    }
}
