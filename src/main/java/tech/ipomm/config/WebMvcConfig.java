package tech.ipomm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import tech.ipomm.interceptors.DocumentationInterceptor;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer  {
	@Autowired
	DocumentationInterceptor documentationInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// LogInterceptor apply to all URLs.
		registry.addInterceptor(documentationInterceptor);

	}

}