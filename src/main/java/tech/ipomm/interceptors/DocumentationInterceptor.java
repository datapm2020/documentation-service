package tech.ipomm.interceptors;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import tech.ipomm.docs.config.swagger.ServiceDescriptionUpdater;
import tech.ipomm.utils.Utils;

@Component
public class DocumentationInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	ServiceDescriptionUpdater serviceDescriptionUpdater;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("\n-------- DocumentationInterceptor.preHandle --- ");
		String contextPath = request.getContextPath() + "/";
		if (request.getRequestURI().startsWith(contextPath + "swagger-ui/index.html")
				|| request.getRequestURI().startsWith(contextPath + "swagger-ui.html"))
		{
			Optional<String> subdomain = Utils.getSubdomain(request.getServerName());
			this.serviceDescriptionUpdater.refreshSwaggerConfigurations(subdomain);
		}
			
		return true;
	}

}