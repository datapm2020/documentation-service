package tech.ipomm.utils;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Utils {

	public static String DOMAIN;
	@Value("${server.domain}")
	public void setDomain(String domain) {
		Utils.DOMAIN = domain;
	}

	public static Optional<String> getSubdomain(String host) {
		if (host.equals(Utils.DOMAIN))
			return Optional.empty();
		else {
			String[] nodes = host.split("\\.");
			return Optional.of(nodes[0]);
		}
	}

}
