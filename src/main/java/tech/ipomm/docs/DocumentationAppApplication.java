package tech.ipomm.docs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan("tech.*")
@EnableScheduling
@EnableFeignClients("tech.ipomm.traefik.api")
public class DocumentationAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(DocumentationAppApplication.class, args);
	}
}