package com.ipomm.docs;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tech.ipomm.docs.DocumentationAppApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentationAppApplication.class)
public class DocumentationAppApplicationTests {
	
	@Test
	public void contextLoads() {

	}

}
