package tech.ipomm.traefik.api;


public class HttpRouter {
	public HttpRouter(String provider, String service) {
		super();
		this.provider = provider;
		this.service = service;
	}
	public HttpRouter() {
		super();
	}
	String provider;
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	String service;
}
